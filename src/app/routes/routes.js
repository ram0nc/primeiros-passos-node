const LivroDao = require('../infra/livro-dao');
const db = require('../../config/database');

module.exports = (app) => {

    app.get('/', function (req, resp) {
        resp.send(`
        <html>
      
        <body> Casa do codigo</body> </html>
        `);
    });

    app.get('/livros', function (req, resp) {
        console.log('listagem livros');
        const livroDao = new LivroDao(db);
        livroDao.lista()
            .then(livros => resp.marko(
                require('../views/livros/lists/listagem.marko'),
                {
                    livros: livros
                }
            ))
            .catch(erro => console.log(erro));

    });


    app.get('/livros/form', function(req,resp) {
        resp.marko(require('../views/livros/form/form.marko'), {livro: {}});
    });

    app.get('/livros/form/:id', function(req,resp) {
        const id = req.params.id;
        console.log("cheguei aqui louco");
        const livroDao = new LivroDao(db);
        livroDao.buscarPorId(id)
            .then(livro => resp.marko(
                require('../views/livros/form/form.marko'),
                {
                    livro: livro
                }
            ))
            .catch(erro => console.log(erro));
    });


    app.post('/livros', function(req,resp) {
  
        console.log(req.body);
        const livroDao = new LivroDao(db);
        livroDao.adiciona(req.body)
                .then(resp.redirect('/livros'))
                .catch(erro => console.log(erro));
    });
   
    app.put('/livros', function(req,resp) {
  
        console.log(req.body);
        const livroDao = new LivroDao(db);
        livroDao.update(req.body)
                .then(resp.redirect('/livros'))
                .catch(erro => console.log(erro));
    });
   
    
    app.delete('/livros/:id', function(req,resp) {
        const id = req.params.id;
        console.log("estou no comando");
        const livroDao = new LivroDao(db);
        livroDao.remove(id)
                .then(() => resp.status(200).end())
                .catch(erro => console.log(erro));
    });
   



};


