class LivroDao {

    constructor(db) {
        this._db = db;

    }

    lista(){
        return new Promise((resolse,reject) =>  { 
            this._db.all('SELECT *FROM livros',
            (erro,resultados) => {
                if (erro) return reject('deu ruim!');
                return resolse(resultados);
            });
        });
     
    }

    adiciona(livro) {
        return new Promise((resolve, reject) => {
            this._db.run(`
            INSERT INTO livros (titulo, preco, descricao) 
            values (?,?,?)
            `, [livro.titulo,
                livro.preco,
                livro.descricao], 
                function(err) {
                    if(err){
                        console.log(err);
                        return reject('nao foi possivel salver');
                    }
                    return resolve();

                })
        });
    }

    buscarPorId(id) {
        return new Promise((resolve, reject) => {
            this._db.get(`
            select *from livros  
            where id = ?
            `, [id], 
                (erro, livro) => {
                    if(erro){
                        console.log(erro);
                        return reject('nao foi possivel buscar');
                    }
                    return resolve(livro);

                });
        });
    }

    remove(id) {
        return new Promise((resolve, reject) => {
            this._db.run(`
            delete from livros  
            where id = ?
            `, [id], 
                function(err) {
                    if(err){
                        console.log(err);
                        return reject('nao foi possivel buscar');
                    }
                    return resolve();

                })
        });
    }

    update(livro) {
        return new Promise((resolve, reject) => {
            this._db.run(`
            update livros set
            titulo = ?,
            preco = ?,
            descricao = ?
            where id = ?
            `, [livro.titulo,livro.preco,livro.descricao,livro.id], 
                function(err) {
                    if(err){
                        console.log(err);
                        return reject('nao foi possivel atualizar');
                    }
                    return resolve();

                });
        });
    }
}

module.exports = LivroDao;